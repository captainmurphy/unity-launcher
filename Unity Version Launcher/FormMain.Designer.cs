﻿namespace Unity_Version_Launcher
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.cbxInstalls = new System.Windows.Forms.ComboBox();
            this.fbdFindFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.txbFolder = new System.Windows.Forms.TextBox();
            this.btnFindFolder = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnRefreshList = new System.Windows.Forms.Button();
            this.bwbSearchingFiles = new System.ComponentModel.BackgroundWorker();
            this.pnlLoading = new System.Windows.Forms.Panel();
            this.pnlLoading.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbxInstalls
            // 
            this.cbxInstalls.FormattingEnabled = true;
            this.cbxInstalls.Location = new System.Drawing.Point(3, 3);
            this.cbxInstalls.Name = "cbxInstalls";
            this.cbxInstalls.Size = new System.Drawing.Size(223, 21);
            this.cbxInstalls.TabIndex = 0;
            this.cbxInstalls.SelectedValueChanged += new System.EventHandler(this.cbxInstalls_SelectedValueChanged);
            // 
            // txbFolder
            // 
            this.txbFolder.Location = new System.Drawing.Point(6, 41);
            this.txbFolder.Name = "txbFolder";
            this.txbFolder.Size = new System.Drawing.Size(223, 20);
            this.txbFolder.TabIndex = 1;
            // 
            // btnFindFolder
            // 
            this.btnFindFolder.Location = new System.Drawing.Point(235, 39);
            this.btnFindFolder.Name = "btnFindFolder";
            this.btnFindFolder.Size = new System.Drawing.Size(75, 23);
            this.btnFindFolder.TabIndex = 2;
            this.btnFindFolder.Text = "Browse...";
            this.btnFindFolder.UseVisualStyleBackColor = true;
            this.btnFindFolder.Click += new System.EventHandler(this.btnFindFolder_Click);
            // 
            // btnStart
            // 
            this.btnStart.Enabled = false;
            this.btnStart.Location = new System.Drawing.Point(232, 1);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 3;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnRefreshList
            // 
            this.btnRefreshList.Location = new System.Drawing.Point(235, 68);
            this.btnRefreshList.Name = "btnRefreshList";
            this.btnRefreshList.Size = new System.Drawing.Size(75, 23);
            this.btnRefreshList.TabIndex = 4;
            this.btnRefreshList.Text = "Refresh";
            this.btnRefreshList.UseVisualStyleBackColor = true;
            this.btnRefreshList.Click += new System.EventHandler(this.btnRefreshList_Click);
            // 
            // bwbSearchingFiles
            // 
            this.bwbSearchingFiles.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwbSearchingFiles_DoWork);
            this.bwbSearchingFiles.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwbSearchingFiles_RunWorkerCompleted);
            // 
            // pnlLoading
            // 
            this.pnlLoading.Controls.Add(this.cbxInstalls);
            this.pnlLoading.Controls.Add(this.btnRefreshList);
            this.pnlLoading.Controls.Add(this.btnStart);
            this.pnlLoading.Controls.Add(this.btnFindFolder);
            this.pnlLoading.Controls.Add(this.txbFolder);
            this.pnlLoading.Location = new System.Drawing.Point(6, 5);
            this.pnlLoading.Name = "pnlLoading";
            this.pnlLoading.Size = new System.Drawing.Size(317, 100);
            this.pnlLoading.TabIndex = 5;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(333, 117);
            this.Controls.Add(this.pnlLoading);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMain";
            this.Text = "Unity Startup";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.pnlLoading.ResumeLayout(false);
            this.pnlLoading.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbxInstalls;
        private System.Windows.Forms.FolderBrowserDialog fbdFindFolder;
        private System.Windows.Forms.TextBox txbFolder;
        private System.Windows.Forms.Button btnFindFolder;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnRefreshList;
        private System.ComponentModel.BackgroundWorker bwbSearchingFiles;
        private System.Windows.Forms.Panel pnlLoading;
    }
}

