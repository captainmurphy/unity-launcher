﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Runtime.Serialization.Formatters.Binary;
using IWshRuntimeLibrary;

namespace Unity_Version_Launcher
{
    public partial class FormMain : Form
    {
        //string lastVersion = "";
        List<UnityInstall> installs = new List<UnityInstall>();
        string programsPath = "";
        public FormMain()
        {
            InitializeComponent();
        }

        #region functions

        private void refreshInstalls ()
        {
            if (installs.Count > 0)
            {
                foreach (UnityInstall ui in installs)
                {
                    cbxInstalls.Items.Add(ui);
                }

                if (Properties.Settings.Default.LastRun != null)
                {
                    string tmp = Properties.Settings.Default.LastRun;
                    foreach (UnityInstall cb in cbxInstalls.Items)
                    {
                        if (cb.name == tmp)
                        {
                            cbxInstalls.SelectedItem = cb;
                            break;
                        }
                    }
                    btnStart.Enabled = true;
                }
            }
        }

        #endregion

        private void FormMain_Load(object sender, EventArgs e)
        {
            installs = LoadInstalls();
            //lastVersion = (Properties.Settings.Default.LastRun != null) ? Properties.Settings.Default.LastRun : "";
            refreshInstalls();


            if (Properties.Settings.Default.ProgramsFolder != null)
            {
                if (Directory.Exists(Properties.Settings.Default.ProgramsFolder))
                {
                    txbFolder.Text = Properties.Settings.Default.ProgramsFolder;
                    programsPath = Properties.Settings.Default.ProgramsFolder;
                }
            }
        }

        private void btnFindFolder_Click(object sender, EventArgs e)
        {
            fbdFindFolder.Description = "Select parent folder for Unity installations";
            if (Directory.Exists(Properties.Settings.Default.ProgramsFolder))
            {
                fbdFindFolder.SelectedPath = Properties.Settings.Default.ProgramsFolder;
            }
            if (fbdFindFolder.ShowDialog() == DialogResult.OK)
            {
                Properties.Settings.Default.ProgramsFolder = fbdFindFolder.SelectedPath;
                txbFolder.Text = fbdFindFolder.SelectedPath;
                programsPath = fbdFindFolder.SelectedPath;
                Properties.Settings.Default.Save();
            }
        }

        private void btnRefreshList_Click(object sender, EventArgs e)
        {
            if (!bwbSearchingFiles.IsBusy)
            {
                Cursor = Cursors.WaitCursor;
                cbxInstalls.Items.Clear();
                cbxInstalls.Text = "Searching...";
                pnlLoading.Enabled = false;
                bwbSearchingFiles.RunWorkerAsync();
            }
        }

        void SaveUnityInstalls(List<UnityInstall> installs)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, installs);
                ms.Position = 0;
                byte[] buffer = new byte[(int)ms.Length];
                ms.Read(buffer, 0, buffer.Length);
                Properties.Settings.Default.Installs = Convert.ToBase64String(buffer);
                Properties.Settings.Default.Save();
            }
        }

        List<UnityInstall> LoadInstalls()
        {
            try
            {
                if (Properties.Settings.Default.Installs != null)
                {
                    using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(Properties.Settings.Default.Installs)))
                    {
                        BinaryFormatter bf = new BinaryFormatter();
                        return (List<UnityInstall>)bf.Deserialize(ms);
                    }
                }
                return new List<UnityInstall>();
            }
            catch (Exception)
            {
                return new List<UnityInstall>();
            }

        }

        private void bwbSearchingFiles_DoWork(object sender, DoWorkEventArgs e)
        {
            if (programsPath != null)
            {
                if (Directory.Exists(programsPath))
                {
                    string[] files = Directory.GetFiles(programsPath + "\\", "*unity.exe", SearchOption.AllDirectories);
                    installs = new List<UnityInstall>();
                    foreach (string f in files)
                    {
                        FileVersionInfo fi = FileVersionInfo.GetVersionInfo(f);
                        if (fi.FileDescription == "Unity Editor")
                        {
                            installs.Add(new UnityInstall(fi.FileVersion, f));
                        }
                    }
                    SaveUnityInstalls(installs);
                }
            }
        }

        private void bwbSearchingFiles_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            cbxInstalls.Text = "";
            if (installs.Count > 0)
            {

                refreshInstalls();
            } else
            {
                cbxInstalls.Text = "No Installs Found";
            }
            pnlLoading.Enabled = true;
            Cursor = Cursors.AppStarting;
        }

        private void cbxInstalls_SelectedValueChanged(object sender, EventArgs e)
        {
            UnityInstall ui = (UnityInstall)cbxInstalls.SelectedItem;
            if (System.IO.File.Exists(ui.path))
            {
                btnStart.Enabled = true;
                setDefaultInstall(ui.name);
            }
        }

        private void setDefaultInstall(string version)
        {
            Properties.Settings.Default.LastRun = version;
            Properties.Settings.Default.Save();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            UnityInstall ui = (UnityInstall)cbxInstalls.SelectedItem;
            if (System.IO.File.Exists(ui.path))
            {
                setDefaultInstall(ui.name);

                Process p = new Process();
                p.StartInfo.FileName = ui.path;
                p.StartInfo.WorkingDirectory = ui.path;
                p.Start();
            }
        }
    }

    [Serializable]
    class UnityInstall
    {
        public string name;
        public string path;

        public UnityInstall(string _name, string _path)
        {
            name = _name;
            path = _path;
        }
        public override string ToString()
        {
            return name;
        }

        public void CreateShortcut(string _pathForShortcut)
        {
            WshShell wsh = new WshShell();
            IWshRuntimeLibrary.IWshShortcut shortcut = wsh.CreateShortcut(
                Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\shorcut.lnk") 
              as IWshRuntimeLibrary.IWshShortcut;
            shortcut.Arguments = "c:\\app\\settings1.xml";
            shortcut.TargetPath = "c:\\app\\myftp.exe";
            // not sure about what this is for
            shortcut.WindowStyle = 1;
            shortcut.Description = "my shortcut description";
            shortcut.WorkingDirectory = "c:\\app";
            shortcut.IconLocation = "specify icon location";
            shortcut.Save();
        }
    }

    class Item
    {
        public string Name;
        public string Value;
        public Item(string name, string value)
        {
            Name = name;
            Value = value;
        }
        public override string ToString()
        {
            // Generates the text shown in the combo box
            return Name;
        }
    }
}
